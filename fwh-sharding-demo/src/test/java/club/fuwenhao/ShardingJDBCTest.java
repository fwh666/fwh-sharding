package club.fuwenhao;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import club.fuwenhao.entity.Course;
import club.fuwenhao.entity.Dict;
import club.fuwenhao.entity.User;
import club.fuwenhao.mapper.CourseMapper;
import club.fuwenhao.mapper.DictMapper;
import club.fuwenhao.mapper.UserMapper;
import org.apache.shardingsphere.api.hint.HintManager;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import javax.annotation.Resource;
import java.util.List;

/**
 * @author ：楼兰
 * @date ：Created in 2021/1/4
 * @description:
 **/

@RunWith(SpringRunner.class)
@SpringBootTest
public class ShardingJDBCTest {
    @Resource
    CourseMapper courseMapper;
    @Resource
    DictMapper dictMapper;
    @Resource
    UserMapper userMapper;

    /**
     * 添加课程
     */
    @Test
    public void addCourse() {
        for (int i = 0; i < 10; i++) {
            Course c = new Course();
//            c.setCid(Long.valueOf(i));
            c.setCname("shardingsphere");
            c.setUserId(Long.valueOf("" + (1000 + i)));
            c.setCstatus("1");
            courseMapper.insert(c);
        }
    }

    /**
     * 精准查询
     */
    @Test
    public void queryCourse() {
        //select * from course
        QueryWrapper<Course> wrapper = new QueryWrapper<>();
        wrapper.orderByDesc("cid");
        wrapper.eq("cid", 663326184112656384L);
//        wrapper.in()
        List<Course> courses = courseMapper.selectList(wrapper);
        courses.forEach(course -> System.out.println(course));
    }

    /**
     * 范围查询
     */
    @Test
    public void queryOrderRange() {
        //select * from course
        QueryWrapper<Course> wrapper = new QueryWrapper<>();
        wrapper.between("cid", 553684818806706177L, 553684819184193537L);
//        wrapper.in()
        List<Course> courses = courseMapper.selectList(wrapper);
        courses.forEach(course -> System.out.println(course));
    }

    /**
     * complex： 复杂的； 根据SQL来的。
     * <p>
     * CId的策略 减少查询表的操作。可以简化查询一张表得到同样的结果
     */
    @Test
    public void queryCourseComplex() {
        QueryWrapper<Course> wrapper = new QueryWrapper<>();
        wrapper.between("cid", 553684818806706177L, 553684819184193537L);
        wrapper.eq("user_id", 1009L);
//        wrapper.in()
        List<Course> courses = courseMapper.selectList(wrapper);
        courses.forEach(course -> System.out.println(course));
    }


    /**
     * 强制的查询数据；根据分片键去查询(只单独查询course2表，不查询1表)
     * Hint：强制的
     */
    @Test
    public void queryCourseByHint() {
        HintManager hintManager = HintManager.getInstance();
        hintManager.addTableShardingValue("course", 2);
        List<Course> courses = courseMapper.selectList(null);
        courses.forEach(course -> System.out.println(course));
        hintManager.close();
    }


    /**
     * 广播表-数据插入；
     * 指公共用的表结构，每个库都会存在这张表。 都保留全量的数据。
     */
    @Test
    public void addDict() {
        Dict d1 = new Dict();
        d1.setUstatus("1");
        d1.setUvalue("正常");
        dictMapper.insert(d1);

        Dict d2 = new Dict();
        d2.setUstatus("0");
        d2.setUvalue("不正常");
        dictMapper.insert(d2);

        for (int i = 0; i < 10; i++) {
            User user = new User();
            user.setUsername("user No " + i);
            user.setUstatus("" + (i % 2));
            user.setUage(i * 10);
            userMapper.insert(user);
        }
    }

    /**
     * 绑定表概念--解决笛卡尔积的查询问题。
     * 会把计算值相同的表绑定一起
     * 查询用户状态;
     *
     */
    @Test
    public void queryUserStatus() {
        List<User> users = userMapper.queryUserStatus();
        users.forEach(user -> System.out.println(user));
    }

    /**
     * 读写分离-写入指定的主库
     */
    @Test
    public void addDictByMS() {
        Dict d1 = new Dict();
        d1.setUstatus("1");
        d1.setUvalue("正常");
        dictMapper.insert(d1);

        Dict d2 = new Dict();
        d2.setUstatus("0");
        d2.setUvalue("不正常");
        dictMapper.insert(d2);
    }

    /**
     * 读写分离-读取指定的从库
     */
    @Test
    public void queryDictByMS() {
        List<Dict> dicts = dictMapper.selectList(null);
        dicts.forEach(dict -> System.out.println(dict));
    }

}
