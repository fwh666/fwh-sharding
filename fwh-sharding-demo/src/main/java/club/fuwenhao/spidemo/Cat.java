package club.fuwenhao.spidemo;

/**
 * @program: fwh-sharding
 * @description: 实现类
 * @author: fwh
 * @date: 2021-11-30 13:57
 **/
public class Cat implements Animal{
    @Override
    public void sound() {
        System.out.println("喵喵喵");
    }
}
