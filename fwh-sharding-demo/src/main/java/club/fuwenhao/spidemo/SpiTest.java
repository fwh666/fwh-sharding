package club.fuwenhao.spidemo;

import java.util.ServiceLoader;

/**
 * @program: fwh-sharding
 * @description: 测试类
 * @author: fwh
 * @date: 2021-11-30 13:58
 **/
public class SpiTest {
    /**
     * SPI加载使用方法
     *
     * @param args
     * @return void
     * @author fuwenhao
     * @date 2021/11/30 2:01 下午
     */
    public static void main(String[] args) {
        ServiceLoader<Animal> animals = ServiceLoader.load(Animal.class);
        animals.forEach(animal -> {
            animal.sound();
        });
    }
}
