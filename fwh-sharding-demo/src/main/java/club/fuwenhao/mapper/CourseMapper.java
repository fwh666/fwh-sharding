package club.fuwenhao.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import club.fuwenhao.entity.Course;

/**
 * @author ：楼兰
 * @date ：Created in 2021/1/4
 * @description:
 **/

public interface CourseMapper extends BaseMapper<Course> {
}
